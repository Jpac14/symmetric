import { Database } from '@/types/supabase';
import { ChevronDownIcon } from '@chakra-ui/icons';
import { LinkProps } from '@chakra-ui/next-js';
import {
  Box,
  Button,
  Flex,
  HStack,
  Heading,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
} from '@chakra-ui/react';
import { useSupabaseClient, useUser } from '@supabase/auth-helpers-react';
import { useRouter } from 'next/router';
import { MdLogout, MdSettings } from 'react-icons/md';
import ColorModeSwitcher from './color-mode-switcher';
import LinkWithoutUnderline from './link-without-underline';
import SymmetricLogo from './symmetric-logo';

const NavLink = ({ children, ...props }: LinkProps) => (
  <LinkWithoutUnderline
    px={2}
    py={1}
    rounded="md"
    _hover={{
      bg: 'gray.200',
    }}
    _dark={{
      _hover: {
        bg: 'whiteAlpha.200',
      },
    }}
    {...props}
  >
    {children}
  </LinkWithoutUnderline>
);

// TODO: State doesn't refresh on email change
export default function Navbar() {
  const router = useRouter();
  const supabase = useSupabaseClient<Database>();
  const user = useUser();

  async function signOut() {
    await supabase.auth.signOut();
    router.push('/auth/signin');
  }

  return (
    <Box w="full" px={4} border="gray.600" borderBottomWidth={1}>
      <Flex h={14} alignItems="center" justifyContent="space-between">
        <HStack spacing={8} alignItems="center">
          <HStack spacing={4}>
            <SymmetricLogo size={44} />
            <Heading fontSize="2xl">Symmetric</Heading>
          </HStack>
          <HStack spacing={4}>
            {user && <NavLink href="/dashboard">Dashboard</NavLink>}
            <NavLink href="https://docs.symmetric.run" target="_blank">
              Documentation
            </NavLink>
          </HStack>
        </HStack>
        <Flex gap={2} alignItems="center">
          <ColorModeSwitcher />
          {user ? (
            <Menu>
              <MenuButton
                as={Button}
                cursor="pointer"
                variant="ghost"
                rightIcon={<ChevronDownIcon />}
              >
                {user.email}
              </MenuButton>
              <MenuList>
                <MenuItem
                  as={LinkWithoutUnderline}
                  href="/dashboard/settings"
                  icon={<MdSettings size={18} />}
                >
                  Settings
                </MenuItem>
                <MenuItem
                  color="red.400"
                  icon={<MdLogout size={18} />}
                  onClick={async () => await signOut()}
                >
                  Sign Out
                </MenuItem>
              </MenuList>
            </Menu>
          ) : (
            <>
              <Button
                as={LinkWithoutUnderline}
                href="/auth/signin"
                variant="ghost"
                fontWeight="normal"
              >
                Sign In
              </Button>
              <Button
                as={LinkWithoutUnderline}
                href="/auth/signup"
                variant="outline"
              >
                Sign Up
              </Button>
            </>
          )}
        </Flex>
      </Flex>
    </Box>
  );
}
