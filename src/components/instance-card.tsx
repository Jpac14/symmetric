import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Box,
  Button,
  Code,
  Divider,
  Grid,
  HStack,
  IconButton,
  Link,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  Tooltip,
  VStack,
  chakra,
  useDisclosure,
} from '@chakra-ui/react';
import { ReactNode, useRef, useState } from 'react';
import { BsDeviceHdd, BsGpuCard } from 'react-icons/bs';
import { FaMemory } from 'react-icons/fa';
import { HiStatusOnline } from 'react-icons/hi';
import {
  MdAccountBox,
  MdDelete,
  MdKey,
  MdLocationCity,
  MdMemory,
  MdPlayArrow,
  MdPowerSettingsNew,
  MdRefresh,
  MdSettingsEthernet,
  MdStop,
} from 'react-icons/md';

interface SpecLineProps {
  icon: ReactNode;
  spec: ReactNode;
  value: ReactNode;
}

function SpecLine(props: SpecLineProps) {
  return (
    <>
      {props.icon}
      {props.spec}
      <chakra.div justifySelf="end">{props.value}</chakra.div>
    </>
  );
}

export interface InstanceCardProps {
  hostname: string;
  location: string;
  vcpus: string;
  memory: string;
  size: string;
  gpu: string;
  ip: string;
  username: string;
  password: string;
  status: string;
  pricePerHour: string;
  start: () => Promise<void>;
  stop: () => Promise<void>;
  reboot: () => Promise<void>;
  terminate: () => Promise<void>;
}

export default function InstanceCard(props: InstanceCardProps) {
  const [show, setShow] = useState(false);

  const terminateDisclosure = useDisclosure();
  const connectDisclosure = useDisclosure();
  const cancelRef = useRef<HTMLButtonElement | null>(null);

  return (
    <>
      <Box
        rounded="lg"
        border="gray.600"
        borderWidth={1}
        boxShadow="lg"
        py={6}
        px={8}
      >
        <VStack gap={3}>
          <HStack>
            <Code fontSize="md" mr={6}>
              {props.hostname}
            </Code>
            <IconButton
              color="red.400"
              ml="auto"
              icon={<MdDelete size={24} />}
              aria-label="Delete instance"
              onClick={terminateDisclosure.onOpen}
            />
            <Menu>
              <MenuButton
                as={IconButton}
                icon={<MdPowerSettingsNew size={24} />}
                aria-label="Power instance"
              />
              <MenuList>
                <MenuItem
                  onClick={props.start}
                  icon={<MdPlayArrow size={22} />}
                >
                  Start
                </MenuItem>
                <MenuItem onClick={props.stop} icon={<MdStop size={22} />}>
                  Stop
                </MenuItem>
                <MenuItem onClick={props.reboot} icon={<MdRefresh size={22} />}>
                  Reboot
                </MenuItem>
              </MenuList>
            </Menu>
          </HStack>
          <Divider />
          <Grid
            w="full"
            templateColumns="auto auto 1fr"
            rowGap={1.5}
            columnGap={4}
          >
            <SpecLine
              icon={<MdLocationCity size={22} />}
              spec="Location"
              value={props.location}
            />
            <SpecLine
              icon={<MdMemory size={22} />}
              spec="vCPUs"
              value={props.vcpus}
            />
            <SpecLine
              icon={<FaMemory size={22} />}
              spec="Memory"
              value={props.memory}
            />
            <SpecLine
              icon={<BsDeviceHdd size={22} />}
              spec="Storage"
              value={props.size}
            />
            <SpecLine
              icon={<BsGpuCard size={22} />}
              spec="GPU"
              value={props.gpu}
            />
            <SpecLine
              icon={<MdSettingsEthernet size={22} />}
              spec="IP"
              value={
                <Tooltip label="Click to copy">
                  <Code
                    onClick={() => navigator.clipboard.writeText(props.ip)}
                    cursor="pointer"
                  >
                    {props.ip}
                  </Code>
                </Tooltip>
              }
            />
            <SpecLine
              icon={<MdAccountBox size={22} />}
              spec="Username"
              value={
                <Tooltip label="Click to copy">
                  <Code
                    onClick={() =>
                      navigator.clipboard.writeText(props.username)
                    }
                    cursor="pointer"
                  >
                    {props.username}
                  </Code>
                </Tooltip>
              }
            />
            <SpecLine
              icon={<MdKey size={22} />}
              spec="Password"
              value={
                show ? (
                  <HStack>
                    <Code>{props.password}</Code>
                    <Button
                      size="sm"
                      variant="link"
                      onClick={() => setShow(false)}
                    >
                      Hide
                    </Button>
                  </HStack>
                ) : (
                  <HStack>
                    <Button
                      size="sm"
                      variant="link"
                      onClick={() => setShow(true)}
                    >
                      Show
                    </Button>
                    <Text>or</Text>
                    <Button
                      size="sm"
                      variant="link"
                      onClick={() =>
                        navigator.clipboard.writeText(props.password)
                      }
                    >
                      Copy
                    </Button>
                  </HStack>
                )
              }
            />
            <SpecLine
              icon={<HiStatusOnline size={22} />}
              spec="Status"
              value={props.status}
            />
          </Grid>
          <Divider />
          <VStack w="full" gap={1.5}>
            <Button
              w="full"
              fontWeight="normal"
              onClick={connectDisclosure.onOpen}
            >
              Connect to Instance
            </Button>
            <Text fontSize="xs">${props.pricePerHour}/hr</Text>
          </VStack>
        </VStack>
      </Box>

      <AlertDialog
        isOpen={terminateDisclosure.isOpen}
        leastDestructiveRef={cancelRef}
        onClose={terminateDisclosure.onClose}
        isCentered
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Delete Instance
            </AlertDialogHeader>
            <AlertDialogBody>
              Are you sure? You can&apos;t undo this action afterwards.
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button ref={cancelRef} onClick={terminateDisclosure.onClose}>
                Cancel
              </Button>
              {/* TODO: Loading state */}
              <Button
                colorScheme="red"
                onClick={async () => {
                  terminateDisclosure.onClose();
                  await props.terminate();
                }}
                ml={3}
              >
                Delete
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>

      <Modal
        isOpen={connectDisclosure.isOpen}
        onClose={connectDisclosure.onClose}
        isCentered
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>How to connect</ModalHeader>
          <ModalCloseButton />
          {props.status !== 'running' ? (
            <>
              <ModalBody>
                In order to connect to a instance, it needs to be running.
              </ModalBody>
              <ModalFooter>
                <Button
                  colorScheme="blue"
                  onClick={async () => {
                    await props.start();
                    connectDisclosure.onClose();
                  }}
                >
                  Start
                </Button>
              </ModalFooter>
            </>
          ) : (
            <>
              <ModalBody>
                <Accordion defaultIndex={0}>
                  <AccordionItem>
                    <h2>
                      <AccordionButton>
                        <Box as="span" flex="1" textAlign="left">
                          Connect with RDP
                        </Box>
                        <AccordionIcon />
                      </AccordionButton>
                    </h2>
                    <AccordionPanel pb={4}>
                      <VStack>
                        <Text>
                          We have written a guide on how to connect with RDP,
                          which you can{' '}
                          <Link href="https://docs.symmetric.run/getting-started/connect-with-rdp">
                            view here.
                          </Link>
                        </Text>
                        <a
                          href={`https://www.symmetric.run/api/rdp?ip=${props.ip}&username=${props.username}`}
                          download={`${props.hostname}.rdp`}
                        >
                          <Button colorScheme="blue">Download RDP File</Button>
                        </a>
                      </VStack>
                    </AccordionPanel>
                  </AccordionItem>
                  <AccordionItem>
                    <h2>
                      <AccordionButton>
                        <Box as="span" flex="1" textAlign="left">
                          Connect with Moonlight
                        </Box>
                        <AccordionIcon />
                      </AccordionButton>
                    </h2>
                    <AccordionPanel pb={4}>
                      We are working on a connection with Moonlight.
                    </AccordionPanel>
                  </AccordionItem>
                </Accordion>
              </ModalBody>
              <ModalFooter />
            </>
          )}
        </ModalContent>
      </Modal>
    </>
  );
}
