import { Link, LinkProps } from '@chakra-ui/next-js';
import { forwardRef } from 'react';

const LinkWithoutUnderline = forwardRef<HTMLAnchorElement, LinkProps>(
  (props, ref) => (
    <Link ref={ref} _hover={{ textDmecoration: 'none' }} {...props} />
  )
);

export default LinkWithoutUnderline;
