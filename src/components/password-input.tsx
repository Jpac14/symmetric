import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import {
  IconButton,
  Input,
  InputGroup,
  InputProps,
  InputRightElement,
} from '@chakra-ui/react';
import { forwardRef, useState } from 'react';

export interface PasswordInputProps extends InputProps {}

const PasswordInput = forwardRef<HTMLInputElement, PasswordInputProps>(
  function PasswordInput(props, ref) {
    const [show, setShow] = useState(false);

    return (
      <InputGroup>
        <Input type={show ? 'text' : 'password'} {...props} ref={ref} />
        <InputRightElement>
          <IconButton
            onClick={() => setShow((show) => !show)}
            variant="text"
            aria-label={show ? 'Hide Password' : 'Show Password'}
            icon={show ? <ViewOffIcon /> : <ViewIcon />}
          />
        </InputRightElement>
      </InputGroup>
    );
  }
);

export default PasswordInput;
