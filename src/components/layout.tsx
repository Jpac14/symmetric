import Navbar from '@/components/navbar';
import { Flex } from '@chakra-ui/react';
import Head from 'next/head';

// TODO: Take inspiration from https://github.com/chakra-ui/chakra-ui/blob/main/examples/next-pages/components/Layout.tsx
// TODO: use next-seo
export default function Layout({ children }: { children: React.ReactNode }) {
  return (
    <div>
      <Head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <title>Symmetric</title>
        {/* TODO: Fix favicon */}
        <link
          rel="icon"
          type="image/x-icon"
          href="/favicon-light.ico"
          media="(prefers-color-scheme: light)"
        />
        <link
          rel="icon"
          type="image/x-icon"
          href="/favicon-dark.ico"
          media="(prefers-color-scheme: dark)"
        />
      </Head>
      <Flex flexDirection="column" h="100vh">
        <Navbar />
        {children}
      </Flex>
    </div>
  );
}
