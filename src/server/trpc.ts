/**
 * This is your entry point to setup the root configuration for tRPC on the server.
 * - `initTRPC` should only be used once per app.
 * - We export only the functionality that we use so we can enforce which base procedures should be used
 *
 * Learn how to create protected base procedures and other things below:
 * @see https://trpc.io/docs/v10/router
 * @see https://trpc.io/docs/v10/procedures
 */
import { TRPCError, initTRPC } from '@trpc/server';
import { Context } from './context';

const t = initTRPC.context<Context>().create();

/**
 * Unprotected procedure
 **/
export const publicProcedure = t.procedure;

export const router = t.router;
export const middleware = t.middleware;

const isAuthed = middleware((opts) => {
  const { ctx } = opts;

  if (!ctx.user.data.user) {
    throw new TRPCError({ code: 'UNAUTHORIZED' });
  }

  return opts.next({
    ctx: {
      supabase: ctx.supabase,
      user: ctx.user.data.user,
    },
  });
});

const ownsInstance = isAuthed.unstable_pipe(async (opts) => {
  const { ctx, rawInput } = opts;

  // TODO: just check if record exists with that instance id
  const { data, error } = await ctx.supabase
    .from('instances')
    .select('aws_instance_id')
    .eq('user_id', ctx.user.id);
  const instanceIds = data?.flatMap((value) => value.aws_instance_id);

  if (!instanceIds?.includes(String(rawInput))) {
    throw new TRPCError({ code: 'UNAUTHORIZED' });
  }

  return opts.next();
});

export const protectedProcedure = publicProcedure.use(isAuthed);
export const instanceRoute = publicProcedure.use(ownsInstance);
