import { Database } from '@/types/supabase';
import { createPagesServerClient } from '@supabase/auth-helpers-nextjs';
import type { inferAsyncReturnType } from '@trpc/server';
import type { CreateNextContextOptions } from '@trpc/server/adapters/next';

export async function createContext({ req, res }: CreateNextContextOptions) {
  const supabase = createPagesServerClient<Database>({ req, res });
  const user = await supabase.auth.getUser();

  return {
    supabase,
    user,
  };
}

export type Context = inferAsyncReturnType<typeof createContext>;
