export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json | undefined }
  | Json[]

export interface Database {
  public: {
    Tables: {
      credit: {
        Row: {
          credit: number | null
          user_id: string
        }
        Insert: {
          credit?: number | null
          user_id: string
        }
        Update: {
          credit?: number | null
          user_id?: string
        }
        Relationships: [
          {
            foreignKeyName: "credit_user_id_fkey"
            columns: ["user_id"]
            isOneToOne: true
            referencedRelation: "users"
            referencedColumns: ["id"]
          }
        ]
      }
      instances: {
        Row: {
          aws_instance_id: string
          hostname: string
          password: string
          user_id: string
        }
        Insert: {
          aws_instance_id: string
          hostname: string
          password: string
          user_id: string
        }
        Update: {
          aws_instance_id?: string
          hostname?: string
          password?: string
          user_id?: string
        }
        Relationships: [
          {
            foreignKeyName: "instances_user_id_fkey"
            columns: ["user_id"]
            isOneToOne: false
            referencedRelation: "credit"
            referencedColumns: ["user_id"]
          }
        ]
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      add_credit: {
        Args: {
          user_id: string
          amount: number
        }
        Returns: undefined
      }
      subtract_credit: {
        Args: {
          user_id: string
          amount: number
        }
        Returns: undefined
      }
    }
    Enums: {
      [_ in never]: never
    }
    CompositeTypes: {
      [_ in never]: never
    }
  }
}
