import { extendTheme } from '@chakra-ui/react';
import { Inter } from 'next/font/google';

const inter = Inter({ variable: '--font-inter', subsets: ['latin'] });

export default extendTheme({
  initialColorMode: 'dark',
  useSystemColorMode: true,
  fonts: {
    heading: 'var(--font-inter)',
    body: 'var(--font-inter)',
  },
});
