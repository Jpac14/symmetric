import { instanceRoute, protectedProcedure, router } from '@/server/trpc';
import { TRPCError } from '@trpc/server';
import { EC2 } from 'aws-sdk';
import { z } from 'zod';

function generateHostname() {
  const charset = '0123456789abcdef';
  let hostname = '';

  for (let i = 0, n = charset.length; i < 10; i++) {
    hostname += charset.charAt(Math.floor(Math.random() * n));
  }

  return `symmetric-${hostname}`;
}

function generatePassword() {
  const charset =
    'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*';
  let password = '';

  for (let i = 0, n = charset.length; i < 10; i++) {
    password += charset.charAt(Math.floor(Math.random() * n));
  }

  return password;
}

function formatUserData(hostname: string, password: string) {
  return `<powershell>
# set admin password
$password = "${password}"
net user Administrator "$password"

# set up autologin
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
Install-Module -Name DSCR_AutoLogon -Force
Import-Module -Name DSCR_AutoLogon
$regPath = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon"
[microsoft.win32.registry]::SetValue($regPath, "AutoAdminLogon", "1")
[microsoft.win32.registry]::SetValue($regPath, "DefaultUserName", "Administrator")
[microsoft.win32.registry]::SetValue($regPath, "DefaultPassword", "$password")

# remove need for ctrl+alt+del on lock screen
$regPath = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System"
[microsoft.win32.registry]::SetValue($regPath, "DisableCAD", 1)

# expand root drive to max available size
$drive_letter = "C"
$size = (Get-PartitionSupportedSize -DriveLetter $drive_letter)
Resize-Partition -DriveLetter $drive_letter -Size $size.SizeMax -ErrorAction SilentlyContinue

# give computer unique name
Rename-Computer -NewName "${hostname}" -Force -ErrorAction SilentlyContinue

# download gpu-updater
$ScriptLocation = "$env:ProgramData\\symmetric\\driver-updater\\aws-nvidia.ps1"
$IconLocation = "$env:ProgramData\\symmetric\\driver-updater\\symmetric.ico"
if((Test-Path -Path $env:ProgramData\\symmetric) -eq $true) {} Else {New-Item -Path $env:ProgramData\\symmetric -ItemType directory | Out-Null}
if((Test-Path -Path $env:ProgramData\\symmetric\\driver-updater) -eq $true) {} Else {New-Item -Path $env:ProgramData\\symmetric\\driver-updater -ItemType directory | Out-Null}
(New-Object System.Net.WebClient).DownloadFile("https://www.symmetric.run/aws-nvidia.ps1", "$ScriptLocation")
(New-Object System.Net.WebClient).DownloadFile("https://www.symmetric.run/favicon-light.ico", "$IconLocation")

# create desktop shortcut
Unblock-File -Path "$ScriptLocation"
$Shell = New-Object -ComObject ("WScript.Shell")
$Shortcut = $Shell.CreateShortcut("C:\\Users\\Administrator\\Desktop\\Update GPU Driver.lnk")
$Shortcut.TargetPath="powershell.exe"
$Shortcut.Arguments='-ExecutionPolicy Bypass -File "C:\\ProgramData\\symmetric\\driver-updater\\aws-nvidia.ps1"'
$Shortcut.WorkingDirectory = "$env:ProgramData\\symmetric\\driver-updater";
$Shortcut.IconLocation = "$IconLocation, 0";
$Shortcut.WindowStyle = 0;
$Shortcut.Description = "Update GPU Driver Shortcut";
$Shortcut.Save()

# block manual nvidia driver downloads
(New-Object System.Net.WebClient).DownloadFile("https://www.symmetric.run/aws-nvidia-hosts", "C:\\Windows\\System32\\drivers\\etc\\hosts")
</powershell>
<persist>true</persist>`;
}

export const instancesRouter = router({
  get: protectedProcedure.query(async ({ ctx }) => {
    const ec2 = new EC2({ region: 'ap-southeast-2' }); // TODO: Dynamic regions

    const { data, error } = await ctx.supabase
      .from('instances')
      .select('aws_instance_id,hostname,password')
      .eq('user_id', ctx.user.id);
    const instanceIds = data?.flatMap((value) => value.aws_instance_id);

    if (instanceIds?.length == 0) {
      return [];
    }

    const describeInstancesRes = await ec2
      .describeInstances({ InstanceIds: instanceIds })
      .promise();
    const instancesDetails =
      describeInstancesRes.Reservations?.flatMap(
        (reservation) => reservation.Instances ?? []
      ) ?? [];

    let instances = [];
    for (const instance of instancesDetails) {
      const instanceType = instance.InstanceType!;
      const volumeIds =
        instance.BlockDeviceMappings?.map((device) => device.Ebs?.VolumeId!) ??
        [];

      const [describeTypeRes, describeEbsRes] = await Promise.all([
        ec2.describeInstanceTypes({ InstanceTypes: [instanceType] }).promise(),
        ec2.describeVolumes({ VolumeIds: volumeIds }).promise(),
      ]);

      const typeDetails = describeTypeRes.InstanceTypes?.[0] ?? {};
      const ebsDetails = describeEbsRes.Volumes ?? [];
      const windowsDetails = data?.find(
        (value) => value.aws_instance_id === instance.InstanceId!
      );

      instances.push({
        id: instance.InstanceId!,
        hostname: windowsDetails?.hostname,
        password: windowsDetails?.password,
        vcpus: typeDetails.VCpuInfo?.DefaultVCpus,
        memory: typeDetails.MemoryInfo?.SizeInMiB,
        size: ebsDetails[0]?.Size,
        gpuManufacturer: typeDetails.GpuInfo?.Gpus?.[0]?.Manufacturer,
        gpuName: typeDetails.GpuInfo?.Gpus?.[0]?.Name,
        ip: instance.PublicIpAddress,
        status: instance.State?.Name,
      });
    }

    return instances;
  }),
  start: instanceRoute.input(z.string()).mutation(async ({ ctx, input }) => {
    const { data, error } = await ctx.supabase
      .from('credit')
      .select('credit')
      .eq('user_id', ctx.user.id)
      .single();

    if (error || data.credit! < 0) {
      throw new TRPCError({
        code: 'BAD_REQUEST',
        message: 'You have no credit. Purchase credit to start.',
      });
    }

    const ec2 = new EC2({ region: 'ap-southeast-2' }); // TODO: Dynamic regions

    await ec2.startInstances({ InstanceIds: [input] }).promise();
  }),
  stop: instanceRoute.input(z.string()).mutation(async ({ input }) => {
    const ec2 = new EC2({ region: 'ap-southeast-2' }); // TODO: Dynamic regions

    await ec2.stopInstances({ InstanceIds: [input] }).promise();
  }),
  reboot: instanceRoute.input(z.string()).mutation(async ({ input }) => {
    const ec2 = new EC2({ region: 'ap-southeast-2' }); // TODO: Dynamic regions

    await ec2.rebootInstances({ InstanceIds: [input] }).promise();
  }),
  create: protectedProcedure
    .input(
      z.object({
        region: z.string(), // TODO: convert to enum type
        type: z.string(), // TODO: convert to enum type
        size: z.number().min(50).max(250),
      })
    )
    .mutation(async ({ ctx, input }) => {
      {
        const { data, error } = await ctx.supabase
          .from('credit')
          .select('credit')
          .eq('user_id', ctx.user.id)
          .single();

        if (error || data.credit! < 0) {
          throw new TRPCError({
            code: 'BAD_REQUEST',
            message: 'You have no credit. Purchase credit to create.',
          });
        }
      }

      // TODO: Only allow one instance to be created
      const ec2 = new EC2({ region: input.region });

      const hostname = generateHostname();
      const password = generatePassword();

      const runInstancesRes = await ec2
        .runInstances({
          MinCount: 1,
          MaxCount: 1,
          InstanceType: input.type,
          ImageId: 'ami-0f931837616e42bc7',
          SecurityGroupIds: ['sg-037ed2e0696c22c6b'],
          UserData: btoa(formatUserData(hostname, password)),
          IamInstanceProfile: {
            Arn: 'arn:aws:iam::492941474608:instance-profile/EC2GPUDriverUpdate-Instance-Profile',
          },
          BlockDeviceMappings: [
            {
              DeviceName: '/dev/sda1',
              Ebs: {
                VolumeSize: input.size,
                VolumeType: 'gp2',
              },
            },
          ],
        })
        .promise();
      const instanceId = runInstancesRes.Instances?.[0].InstanceId;

      const { error } = await ctx.supabase
        .from('instances')
        .insert({
          user_id: ctx.user.id,
          aws_instance_id: instanceId!,
          hostname: hostname,
          password: password,
        })
        .select();

      if (error) {
        throw new TRPCError({
          code: 'INTERNAL_SERVER_ERROR',
          message: error.message,
        });
      }
    }),
  terminate: instanceRoute
    .input(z.string())
    .mutation(async ({ ctx, input }) => {
      const ec2 = new EC2({ region: 'ap-southeast-2' }); // TODO: Dynamic region

      await ec2.terminateInstances({ InstanceIds: [input] }).promise();

      await ctx.supabase
        .from('instances')
        .delete()
        .eq('aws_instance_id', input);
    }),
});
