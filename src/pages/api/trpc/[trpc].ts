import { createContext } from '@/server/context';
import { router } from '@/server/trpc';
import * as trpcNext from '@trpc/server/adapters/next';
import { instancesRouter } from './instances';
import { userRouter } from './user';

const appRouter = router({
  user: userRouter,
  instances: instancesRouter,
});

// export only the type definition of the API
// None of the actual implementation is exposed to the client
export type AppRouter = typeof appRouter;

// export API handler
export default trpcNext.createNextApiHandler({
  router: appRouter,
  createContext: createContext,
});
