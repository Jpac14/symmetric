import { protectedProcedure, router } from '@/server/trpc';

export const userRouter = router({
  credit: protectedProcedure.query(async ({ ctx }) => {
    const { data, error } = await ctx.supabase
      .from('credit')
      .select('credit')
      .eq('user_id', ctx.user.id)
      .limit(1)
      .single();

    return data?.credit ?? 0;
  }),
});
