import type { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { ip, username } = req.query;

  res.send(`full address:s:${ip}
username:s:${username}`);
}
