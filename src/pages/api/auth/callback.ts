import { Database } from '@/types/supabase';
import { createPagesServerClient } from '@supabase/auth-helpers-nextjs';
import type { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { code, next } = req.query;

  if (code) {
    const supabase = createPagesServerClient<Database>({ req, res });
    await supabase.auth.exchangeCodeForSession(String(code));
  }

  res.redirect(String(next ?? '/dashboard'));
}
