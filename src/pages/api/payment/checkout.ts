import { Database } from '@/types/supabase';
import { createPagesServerClient } from '@supabase/auth-helpers-nextjs';
import type { NextApiRequest, NextApiResponse } from 'next';
import { Stripe } from 'stripe';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method !== 'POST') {
    res.setHeader('Allow', 'POST');
    res.status(405).end('Method Not Allowed');
  }

  const stripe = new Stripe(process.env.STRIPE_SECRET_KEY!, {
    apiVersion: '2022-11-15',
  });
  const supabase = createPagesServerClient<Database>({ req, res });

  const user = (await supabase.auth.getUser()).data.user!;
  const amount = Number(req.body['amount']);

  const session = await stripe.checkout.sessions.create({
    customer_email: user.email,
    line_items: [
      {
        quantity: 1,
        price_data: {
          currency: 'AUD',
          product_data: {
            name: 'Credit Charge',
          },
          unit_amount: amount * 100,
        },
      },
    ],
    client_reference_id: user.id,
    mode: 'payment',
    success_url: `${req.headers.origin}/dashboard?paymentSuccess=true`,
    cancel_url: `${req.headers.origin}/dashboard?paymentCanceled=true`,
  });

  res.redirect(303, session.url!);
}
