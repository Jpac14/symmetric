import { Database } from '@/types/supabase';
import { createClient } from '@supabase/supabase-js';
import { buffer } from 'micro';
import type { NextApiRequest, NextApiResponse } from 'next';
import { Stripe } from 'stripe';

export const config = {
  api: {
    bodyParser: false,
  },
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method !== 'POST') {
    res.setHeader('Allow', 'POST');
    res.status(405).end('Method Not Allowed');
  }

  const stripe = new Stripe(process.env.STRIPE_SECRET_KEY!, {
    apiVersion: '2022-11-15',
  });
  const supabase = createClient<Database>(
    process.env.NEXT_PUBLIC_SUPABASE_URL!,
    process.env.SUPABASE_SERVICE_KEY!
  );

  const sig = req.headers['stripe-signature']!;
  const payload = await buffer(req);

  let event: Stripe.Event;
  try {
    event = stripe.webhooks.constructEvent(
      payload,
      sig,
      process.env.STRIPE_WEBHOOK_SECRET!
    );
  } catch (err) {
    res.status(400).send(`Webhook Error: ${err}`);
    return;
  }

  if (event.type === 'checkout.session.completed') {
    const checkoutSession = event.data.object as Stripe.Checkout.Session;

    const { error } = await supabase.rpc('add_credit', {
      user_id: checkoutSession.client_reference_id!,
      amount: checkoutSession.amount_total! / 100,
    });

    if (error) {
      res.status(500).send(`Supabase Error: ${error.message}`);
    }
  }

  res.status(200).end();
}
