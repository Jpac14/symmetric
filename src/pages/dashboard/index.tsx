import InstanceCard from '@/components/instance-card';
import Layout from '@/components/layout';
import LinkWithoutUnderline from '@/components/link-without-underline';
import { trpc } from '@/utils/trpc';
import { AddIcon } from '@chakra-ui/icons';
import {
  Button,
  HStack,
  Spinner,
  Text,
  VStack,
  chakra,
  useToast,
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { MdCreditCard } from 'react-icons/md';

export default function Dashboard() {
  const successToast = useToast({ status: 'success' });
  const errorToast = useToast({ status: 'error' });

  const credit = trpc.user.credit.useQuery();

  const instances = trpc.instances.get.useQuery(undefined, {
    refetchInterval: 3000,
  });

  // TODO: loading state, so they can't press twice
  // TODO: make sure they can't start when already started
  // TODO: Help to connect and tips to wait if not working
  const start = trpc.instances.start.useMutation({
    onSuccess: () => {
      successToast({ title: 'Successfully started' });
      instances.refetch();
    },
    onError: (error) =>
      errorToast({ title: 'Failed to start', description: error.message }),
  });
  const stop = trpc.instances.stop.useMutation({
    onSuccess: () => {
      successToast({ title: 'Successfully stopped' });
      instances.refetch();
    },
    onError: (error) =>
      errorToast({ title: 'Failed to stop', description: error.message }),
  });
  const reboot = trpc.instances.reboot.useMutation({
    onSuccess: () => {
      successToast({ title: 'Successfully rebooted' });
      instances.refetch();
    },
    onError: (error) =>
      errorToast({ title: 'Failed to reboot', description: error.message }),
  });
  const terminate = trpc.instances.terminate.useMutation({
    onSuccess: () => {
      successToast({ title: 'Successfully deleted' });
      instances.refetch();
    },
    onError: (error) =>
      errorToast({ title: 'Failed to delete', description: error.message }),
  });

  const router = useRouter();
  useEffect(() => {
    if (Boolean(router.query.paymentSuccess)) {
      successToast({
        title: 'Payment successful',
      });
    } else if (Boolean(router.query.paymentCancelled)) {
      errorToast({
        title: 'Payment failed',
      });
    }
  }, []);

  return (
    <Layout>
      <HStack
        flexDirection="row-reverse"
        alignSelf="flex-end"
        pr={3}
        pt={3}
        gap={3}
      >
        <Button
          as={LinkWithoutUnderline}
          size="sm"
          leftIcon={<AddIcon />}
          href="/dashboard/create"
        >
          Create Instance
        </Button>
        <Button
          as={LinkWithoutUnderline}
          size="sm"
          leftIcon={<MdCreditCard />}
          href="/dashboard/settings?tabIndex=1"
        >
          Add Credit
        </Button>
        <Text>
          <chakra.span fontWeight="bold">Credit</chakra.span>:{' '}
          {credit.isLoading && 'Loading...'}
          {credit.isError && '$0.00'}
          {credit.isSuccess && `$${credit.data.toFixed(2)}`}
        </Text>
      </HStack>
      <HStack justifyContent="center" h="full" gap={16}>
        {instances.isLoading && (
          <HStack gap={6}>
            <Text fontSize="3xl">Loading...</Text>
            <Spinner
              thickness="4px"
              speed="0.65s"
              emptyColor="gray.200"
              color="blue.500"
              size="lg"
            />
          </HStack>
        )}

        {instances.data?.length === 0 ? (
          <VStack gap={4}>
            <Text fontSize="2xl">You haven&apos;t got any instances</Text>
            <Button
              as={LinkWithoutUnderline}
              size="sm"
              leftIcon={<AddIcon />}
              href="/dashboard/create"
            >
              Create Instance
            </Button>
          </VStack>
        ) : (
          instances.data?.map((instance) => (
            <InstanceCard
              key={instance.id}
              hostname={instance.hostname ?? '-'}
              location="Sydney, Australia"
              vcpus={`${instance.vcpus ?? '-'}`}
              memory={`${instance.memory ? instance.memory / 1024 : '-'} GB`}
              size={`${instance.size ?? '-'} GB`}
              gpu={`${instance.gpuManufacturer} ${instance.gpuName}`}
              ip={instance.ip ?? '-'}
              username="Administrator"
              password={instance.password ?? '-'}
              status={instance.status ?? '-'}
              pricePerHour="0.75"
              start={async () => await start.mutateAsync(instance.id)}
              stop={async () => await stop.mutateAsync(instance.id)}
              reboot={async () => await reboot.mutateAsync(instance.id)}
              terminate={async () => await terminate.mutateAsync(instance.id)}
            />
          ))
        )}
      </HStack>
    </Layout> 
  );
}
