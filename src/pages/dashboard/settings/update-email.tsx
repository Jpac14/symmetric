import Layout from '@/components/layout';
import { Database } from '@/types/supabase';
import {
  Box,
  Button,
  Center,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Input,
  Stack,
  useToast,
} from '@chakra-ui/react';
import { zodResolver } from '@hookform/resolvers/zod';
import { useSupabaseClient } from '@supabase/auth-helpers-react';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

interface FormData {
  email: string;
}

const schema = z.object({
  email: z.string({ required_error: 'Required' }).email('Invalid email'),
});

export default function UpdateEmail() {
  const router = useRouter();
  const supabase = useSupabaseClient<Database>();

  const toast = useToast();

  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm<FormData>({
    defaultValues: {
      email: '',
    },
    resolver: zodResolver(schema),
  });

  async function updateEmail(values: FormData) {
    const { error } = await supabase.auth.updateUser({
      email: values.email,
    });

    if (error) {
      toast({
        status: 'error',
        title: error.message,
      });
    } else {
      toast({
        status: 'warning',
        title: 'Action required to change email',
        description:
          'Please confirm the email change, by click the linking sent to the new and old email!',
        duration: 10000,
      });

      router.push('/dashboard');
    }
  }

  return (
    <Layout>
      <Center as={Box} h="full">
        <Stack spacing={8} w="xs">
          <Heading fontSize="3xl" textAlign="center">
            Update Email
          </Heading>
          <form onSubmit={handleSubmit(updateEmail)}>
            <Stack spacing={4}>
              <FormControl isInvalid={!!errors.email}>
                <FormLabel>Enter your new email</FormLabel>
                <Input {...register('email')} />
                <FormErrorMessage>
                  {errors.email && errors.email.message}
                </FormErrorMessage>
              </FormControl>
              <Button mt={4} isLoading={isSubmitting} type="submit">
                Update Email
              </Button>
            </Stack>
          </form>
        </Stack>
      </Center>
    </Layout>
  );
}
