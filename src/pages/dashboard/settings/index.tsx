import Layout from '@/components/layout';
import LinkWithoutUnderline from '@/components/link-without-underline';
import { trpc } from '@/utils/trpc';
import {
  Button,
  FormControl,
  FormLabel,
  HStack,
  Heading,
  Input,
  InputGroup,
  InputLeftElement,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
  VStack,
  chakra,
} from '@chakra-ui/react';
import { useUser } from '@supabase/auth-helpers-react';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { NumericFormat } from 'react-number-format';

export default function Settings() {
  const [amount, setAmount] = useState(0);

  const router = useRouter();
  const [tabIndex, setTabIndex] = useState(0);

  useEffect(() => {
    const tabIndex = router.query['tabIndex'];
    if (tabIndex) {
      setTabIndex(Number(tabIndex) ?? 0);
    }
  }, []);

  const user = useUser();
  const credit = trpc.user.credit.useQuery();

  return (
    <Layout>
      <Tabs w="full" index={tabIndex} onChange={setTabIndex}>
        <TabList>
          <Tab>Account</Tab>
          <Tab>Credit</Tab>
          <Tab>Charge History</Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <VStack gap={6}>
              <FormControl>
                <FormLabel>Email</FormLabel>
                <HStack w="sm">
                  <Input isDisabled value={user?.email ?? 'Loading...'} />
                  <Button
                    as={LinkWithoutUnderline}
                    colorScheme="blue"
                    href="/dashboard/settings/update-email"
                  >
                    Edit
                  </Button>
                </HStack>
              </FormControl>
              <FormControl>
                <FormLabel>Password</FormLabel>
                <HStack w="sm">
                  <Input isDisabled value="password" type="password" />
                  <Button
                    as={LinkWithoutUnderline}
                    colorScheme="blue"
                    href="/dashboard/settings/update-password"
                  >
                    Edit
                  </Button>
                </HStack>
              </FormControl>
            </VStack>
          </TabPanel>
          <TabPanel>
            <VStack alignItems="start">
              <Text>
                Available Credit:{' '}
                <chakra.span fontWeight="bold">
                  {credit.isLoading
                    ? 'Loading...'
                    : `$${(credit.data ?? 0).toFixed(2)}`}
                </chakra.span>
              </Text>
              <form action="/api/payment/checkout" method="post">
                <HStack gap={3}>
                  <InputGroup>
                    <InputLeftElement pointerEvents="none">$</InputLeftElement>
                    <Input
                      name="amount"
                      value={amount}
                      onChange={(e) => setAmount(Number(e.target.value))}
                      as={NumericFormat}
                      allowNegative={false}
                      decimalScale={2}
                      fixedDecimalScale={true}
                      thousandSeparator=","
                    />
                  </InputGroup>
                  <Button type="submit" colorScheme="blue" px={6}>
                    Add Credit
                  </Button>
                </HStack>
              </form>
              <HStack>
                <Button size="sm" onClick={() => setAmount(10)}>
                  $10
                </Button>
                <Button size="sm" onClick={() => setAmount(25)}>
                  $25
                </Button>
                <Button size="sm" onClick={() => setAmount(50)}>
                  $50
                </Button>
              </HStack>
            </VStack>
          </TabPanel>
          <TabPanel>
            <Heading>Work in progress</Heading>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Layout>
  );
}
