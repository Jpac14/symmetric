import Layout from '@/components/layout';
import PasswordInput from '@/components/password-input';
import { Database } from '@/types/supabase';
import {
  Box,
  Button,
  Center,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Stack,
  useToast,
} from '@chakra-ui/react';
import { zodResolver } from '@hookform/resolvers/zod';
import { useSupabaseClient } from '@supabase/auth-helpers-react';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

interface FormData {
  password: string;
  confirmPassword: string;
}

// TODO: Check password strength
const schema = z
  .object({
    email: z.string({ required_error: 'Required' }).email('Invalid email'),
    password: z
      .string({ required_error: 'Required' })
      .min(1, { message: 'Required' }),
    confirmPassword: z.string(),
  })
  .superRefine(({ password, confirmPassword }, ctx) => {
    if (password !== confirmPassword) {
      ctx.addIssue({
        path: ['confirmPassword'],
        code: 'custom',
        message: 'Passwords do not mathc',
      });
    }
  });

export default function UpdatePassword() {
  const router = useRouter();
  const supabase = useSupabaseClient<Database>();

  const toast = useToast();

  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm<FormData>({
    defaultValues: {
      password: '',
      confirmPassword: '',
    },
    resolver: zodResolver(schema),
  });

  async function updatePassword(values: FormData) {
    const { error } = await supabase.auth.updateUser({
      password: values.password,
    });

    if (error) {
      toast({
        status: 'error',
        title: error.message,
      });
    } else {
      toast({
        status: 'success',
        title: 'Updated password',
      });

      router.push('/dashboard');
    }
  }

  return (
    <Layout>
      <Center as={Box} h="full">
        <Stack spacing={8} w="xs">
          <Heading fontSize="3xl" textAlign="center">
            Update Password
          </Heading>
          <form onSubmit={handleSubmit(updatePassword)}>
            <Stack spacing={4}>
              <FormControl isInvalid={!!errors.password}>
                <FormLabel>Password</FormLabel>
                <PasswordInput {...register('password')} />
                <FormErrorMessage>
                  {errors.password && errors.password.message}
                </FormErrorMessage>
              </FormControl>
              <FormControl isInvalid={!!errors.confirmPassword}>
                <FormLabel>Confirm Password</FormLabel>
                <PasswordInput {...register('confirmPassword')} />
                <FormErrorMessage>
                  {errors.confirmPassword && errors.confirmPassword.message}
                </FormErrorMessage>
              </FormControl>
              <Button mt={4} isLoading={isSubmitting} type="submit">
                Update Password
              </Button>
            </Stack>
          </form>
        </Stack>
      </Center>
    </Layout>
  );
}
