import Layout from '@/components/layout';
import { trpc } from '@/utils/trpc';
import {
  Button,
  Center,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Select,
  Slider,
  SliderFilledTrack,
  SliderMark,
  SliderThumb,
  SliderTrack,
  Text,
  VStack,
  useToast,
} from '@chakra-ui/react';
import { zodResolver } from '@hookform/resolvers/zod';
import { useRouter } from 'next/router';
import { Controller, useForm } from 'react-hook-form';
import { z } from 'zod';

interface FormData {
  region: string;
  family: string;
  type: string;
  size: number;
}

const schema = z.object({
  region: z
    .string({ required_error: 'A region must be selected' })
    .min(0, 'A region must be selected'),
  family: z
    .string({ required_error: 'A family must be selected' })
    .min(0, 'A family must be selected'),
  type: z
    .string({ required_error: 'A type must be selected' })
    .min(0, 'A type must be selected'),
  size: z
    .number({ required_error: 'A storage size must be selected' })
    .min(50, 'Storage size must be greater than 50 GB')
    .max(250, 'Storage size must be less than 250GB'),
});

export default function Create() {
  const toast = useToast();

  const create = trpc.instances.create.useMutation({
    onSuccess: () => {
      toast({ status: 'success', title: 'Created instance' });
      router.push('/dashboard');
    },
    onError: (error) => {
      toast({
        status: 'error',
        title: 'Failed to create instance',
        description: error.message,
      });
    },
  });
  const router = useRouter();

  const {
    handleSubmit,
    register,
    getValues,
    watch,
    control,
    formState: { errors, isSubmitting },
  } = useForm<FormData>({
    defaultValues: {
      region: 'ap-southeast-2',
      family: 'g4dn',
      type: 'xlarge',
      size: 70,
    },
    resolver: zodResolver(schema),
  });

  watch('family'); // Update types on family change

  async function onSubmit(values: FormData) {
    await create.mutateAsync({
      region: values.region,
      type: `${values.family}.${values.type}`,
      size: values.size,
    });
  }

  return (
    <Layout>
      <Center h="full">
        <form onSubmit={handleSubmit(onSubmit)}>
          <VStack gap={4} w="sm">
            <Text fontSize="2xl">Create Instance</Text>
            <FormControl isInvalid={!!errors.region}>
              <FormLabel>Region</FormLabel>
              <Select {...register('region')}>
                <option value="ap-southeast-2">Asia Pacific (Sydney)</option>
              </Select>
              <FormErrorMessage>
                {errors.region && errors.region.message}
              </FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={!!errors.family}>
              <FormLabel>GPU</FormLabel>
              <Select {...register('family')}>
                <option value="g4dn">NVIDIA Tesla T4</option>
                <option value="g5">NVIDIA A10G</option>
              </Select>
              <FormErrorMessage>
                {errors.family && errors.family.message}
              </FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={!!errors.type}>
              <FormLabel>CPU & RAM</FormLabel>
              <Select {...register('type')}>
                {getValues().family === 'g4dn' ? (
                  <>
                    <option value="xlarge">
                      Intel Xeon Platinum 2.5 GHz, 4 vCPUs, 16 GB RAM
                    </option>
                    <option value="2xlarge">
                      Intel Xeon Platinum 2.5 GHz, 8 vCPUs, 32 GB RAM
                    </option>
                  </>
                ) : (
                  <>
                    <option value="xlarge">
                      AMD EPYC 3.3 GHz, 8 vCPUs, 32GB RAM
                    </option>
                    <option value="2xlarge">
                      AMD EPYC 3.3 GHz, 16 vCPUs, 64GB RAM
                    </option>
                  </>
                )}
              </Select>
              <FormErrorMessage>
                {errors.type && errors.type.message}
              </FormErrorMessage>
            </FormControl>
            <FormControl>
              <FormLabel>Storage Size</FormLabel>
              <Controller
                name="size"
                control={control}
                render={({ field }) => (
                  <Slider
                    min={50}
                    max={250}
                    step={10}
                    mb={4}
                    {...field}
                    onChange={(value) => field.onChange(value)}
                  >
                    <SliderMark
                      value={getValues().size}
                      textAlign="center"
                      bg="gray.200"
                      color="black"
                      _dark={{ bg: 'whiteAlpha.200', color: 'white' }}
                      mt={3.5}
                      ml={-8}
                      w={16}
                      px={1}
                      rounded="sm"
                    >
                      {getValues().size} GB
                    </SliderMark>

                    <SliderTrack>
                      <SliderFilledTrack />
                    </SliderTrack>
                    <SliderThumb />
                  </Slider>
                )}
              />
            </FormControl>
            <Button w="full" type="submit" isLoading={isSubmitting}>
              Create
            </Button>
          </VStack>
        </form>
      </Center>
    </Layout>
  );
}
