import Layout from '@/components/layout';
import PasswordInput from '@/components/password-input';
import { Database } from '@/types/supabase';
import { Link } from '@chakra-ui/next-js';
import {
  Box,
  Button,
  Center,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Input,
  Stack,
  Text,
  useToast,
} from '@chakra-ui/react';
import { zodResolver } from '@hookform/resolvers/zod';
import { useSupabaseClient } from '@supabase/auth-helpers-react';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

interface FormData {
  email: string;
  password: string;
}

const schema = z.object({
  email: z.string({ required_error: 'Required' }).email('Invalid email'),
  password: z
    .string({ required_error: 'Required' })
    .min(1, { message: 'Required' }),
});

export default function SignIn() {
  const router = useRouter();
  const supabase = useSupabaseClient<Database>();

  const toast = useToast();

  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm<FormData>({
    defaultValues: {
      email: '',
      password: '',
    },
    resolver: zodResolver(schema),
  });

  async function signIn(values: FormData) {
    const { error } = await supabase.auth.signInWithPassword({
      email: values.email,
      password: values.password,
    });

    if (error) {
      toast({
        status: 'error',
        title: error.message,
      });
    } else {
      router.push('/dashboard');
    }
  }

  // TODO: OAuth providers
  return (
    <Layout>
      <Center as={Box} h="full">
        <Stack spacing={8} w="xs">
          <Stack align="center">
            <Heading fontSize="3xl">Log in to your account</Heading>
            <Text fontSize="lg" color="gray.600" _dark={{ color: 'gray.400' }}>
              Don&apos;t have an account?{' '}
              <Link
                href="/auth/signup"
                color="black"
                _dark={{ color: 'white' }}
              >
                Sign Up
              </Link>
            </Text>
          </Stack>
          <form onSubmit={handleSubmit(signIn)}>
            <Stack spacing={4}>
              <FormControl isInvalid={!!errors.email}>
                <FormLabel>Email</FormLabel>
                <Input {...register('email')} />
                <FormErrorMessage>
                  {errors.email && errors.email.message}
                </FormErrorMessage>
              </FormControl>
              <FormControl isInvalid={!!errors.password}>
                <FormLabel>Password</FormLabel>
                <PasswordInput {...register('password')} />
                <FormErrorMessage>
                  {errors.password && errors.password.message}
                </FormErrorMessage>
              </FormControl>
              <Stack mt={4}>
                <Button isLoading={isSubmitting} type="submit">
                  Sign in
                </Button>
                <Link
                  textAlign="center"
                  fontSize="sm"
                  href="/auth/forgot-password"
                >
                  Forgot Password?
                </Link>
              </Stack>
            </Stack>
          </form>
        </Stack>
      </Center>
    </Layout>
  );
}
