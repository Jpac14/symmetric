import Layout from '@/components/layout';
import { Database } from '@/types/supabase';
import { Link } from '@chakra-ui/next-js';
import {
  Box,
  Button,
  Center,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Input,
  Stack,
  Text,
  useToast,
} from '@chakra-ui/react';
import { zodResolver } from '@hookform/resolvers/zod';
import { useSupabaseClient } from '@supabase/auth-helpers-react';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

interface FormData {
  email: string;
}

const schema = z.object({
  email: z.string({ required_error: 'Required' }).email('Invalid email'),
});

export default function ForgotPassword() {
  const router = useRouter();
  const supabase = useSupabaseClient<Database>();

  const toast = useToast();

  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm<FormData>({
    defaultValues: {
      email: '',
    },
    resolver: zodResolver(schema),
  });

  async function forgotPassword(values: FormData) {
    const { error } = await supabase.auth.resetPasswordForEmail(values.email, {
      redirectTo: `${location.origin}/api/auth/callback?next=/dashboard/settings/update-password`,
    });

    if (error) {
      toast({
        status: 'error',
        title: error.message,
      });
    } else {
      toast({
        status: 'warning',
        title: 'Action required to change password',
        description: 'Check your emails to reset your password!',
        duration: 10000,
      });

      router.push('/');
    }
  }

  return (
    <Layout>
      <Center as={Box} h="full">
        <Stack spacing={8} w="xs">
          <Stack align="center">
            <Heading fontSize="3xl">Forgot Password</Heading>
            <Text fontSize="lg" color="gray.600" _dark={{ color: 'gray.400' }}>
              Just remembered it?{' '}
              <Link
                href="/auth/signin"
                color="black"
                _dark={{ color: 'white' }}
              >
                Sign In
              </Link>
            </Text>
          </Stack>
          <form onSubmit={handleSubmit(forgotPassword)}>
            <Stack spacing={4}>
              <FormControl isInvalid={!!errors.email}>
                <FormLabel>Email</FormLabel>
                <Input {...register('email')} />
                <FormErrorMessage>
                  {errors.email && errors.email.message}
                </FormErrorMessage>
              </FormControl>
              <Button mt={4} isLoading={isSubmitting} type="submit">
                Send Verification Email
              </Button>
            </Stack>
          </form>
        </Stack>
      </Center>
    </Layout>
  );
}
