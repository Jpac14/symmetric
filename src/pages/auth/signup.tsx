import Layout from '@/components/layout';
import PasswordInput from '@/components/password-input';
import { Database } from '@/types/supabase';
import { Link } from '@chakra-ui/next-js';
import {
  Box,
  Button,
  Center,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Input,
  Stack,
  Text,
  useToast,
} from '@chakra-ui/react';
import { zodResolver } from '@hookform/resolvers/zod';
import { useSupabaseClient } from '@supabase/auth-helpers-react';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

interface FormData {
  email: string;
  password: string;
  confirmPassword: string;
}

// TODO: Check password strength
const schema = z
  .object({
    email: z.string({ required_error: 'Required' }).email('Invalid email'),
    password: z
      .string({ required_error: 'Required' })
      .min(1, { message: 'Required' }),
    confirmPassword: z.string(),
  })
  .superRefine(({ password, confirmPassword }, ctx) => {
    if (password !== confirmPassword) {
      ctx.addIssue({
        path: ['confirmPassword'],
        code: 'custom',
        message: 'Passwords do not match',
      });
    }
  });

export default function SignUp() {
  const router = useRouter();
  const supabase = useSupabaseClient<Database>();

  const toast = useToast();

  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm<FormData>({
    defaultValues: {
      email: '',
      password: '',
      confirmPassword: '',
    },
    resolver: zodResolver(schema),
  });

  async function signUp(values: FormData) {
    const { error } = await supabase.auth.signUp({
      email: values.email,
      password: values.password,
      options: {
        emailRedirectTo: `${location.origin}/api/auth/callback`,
      },
    });

    if (error) {
      toast({
        status: 'error',
        title: error.message,
      });
    } else {
      toast({
        status: 'warning',
        title: 'Action required to create account',
        description:
          'Check your emails to verify your account before signing in!',
        duration: 10000,
      });

      router.push('/');
    }
  }

  // TODO: OAuth providers
  return (
    <Layout>
      <Center as={Box} h="full">
        <Stack spacing={8} w="xs">
          <Stack align="center">
            <Heading fontSize="3xl">Create an account</Heading>
            <Text fontSize="lg" color="gray.600" _dark={{ color: 'gray.400' }}>
              Already have an account?{' '}
              <Link
                href="/auth/signin"
                color="black"
                _dark={{ color: 'white' }}
              >
                Sign In
              </Link>
            </Text>
          </Stack>
          <form onSubmit={handleSubmit(signUp)}>
            <Stack spacing={4}>
              <FormControl isInvalid={!!errors.email}>
                <FormLabel>Email</FormLabel>
                <Input {...register('email')} />
                <FormErrorMessage>
                  {errors.email && errors.email.message}
                </FormErrorMessage>
              </FormControl>
              <FormControl isInvalid={!!errors.password}>
                <FormLabel>Password</FormLabel>
                <PasswordInput {...register('password')} />
                <FormErrorMessage>
                  {errors.password && errors.password.message}
                </FormErrorMessage>
              </FormControl>
              <FormControl isInvalid={!!errors.confirmPassword}>
                <FormLabel>Confirm Password</FormLabel>
                <PasswordInput {...register('confirmPassword')} />
                <FormErrorMessage>
                  {errors.confirmPassword && errors.confirmPassword.message}
                </FormErrorMessage>
              </FormControl>
              <Button mt={4} isLoading={isSubmitting} type="submit">
                Sign up
              </Button>
            </Stack>
          </form>
        </Stack>
      </Center>
    </Layout>
  );
}
