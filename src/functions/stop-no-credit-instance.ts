import { Database } from '@/types/supabase';
import { createClient } from '@supabase/supabase-js';
import { EC2 } from 'aws-sdk';

export async function handler() {
  const ec2 = new EC2({ region: 'ap-southeast-2' }); // TODO: Dynamic regions
  const supabase = createClient<Database>(
    process.env.NEXT_PUBLIC_SUPABASE_URL!,
    process.env.SUPABASE_SERVICE_KEY!
  );

  const running = await ec2.describeInstanceStatus().promise();
  const runningIds =
    running.InstanceStatuses?.map((instance) => instance.InstanceId) ?? [];

  const outOfCredit = await supabase
    .from('instances')
    .select('user_id, aws_instance_id, credit(credit)')
    .in('aws_instance_id', runningIds)
    .lt('credit.credit', 0);

  const instancesToStop =
    outOfCredit.data?.map((instance) => instance.aws_instance_id) ?? [];

  await ec2.stopInstances({ InstanceIds: instancesToStop }).promise();
}
