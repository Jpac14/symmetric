import { Database } from '@/types/supabase';
import { createClient } from '@supabase/supabase-js';
import { EC2 } from 'aws-sdk';

// TODO: dynamic pricing
const PRICE_PER_MINUTE = 0.01; // $0.01 per minute

export async function handler() {
  const ec2 = new EC2({ region: 'ap-southeast-2' }); // TODO: Dynamic regions
  const supabase = createClient<Database>(
    process.env.SUPABASE_URL!,
    process.env.SUPABASE_SERVICE_KEY!
  );

  const running = await ec2.describeInstanceStatus().promise();
  const instanceIds =
    running.InstanceStatuses?.map((instance) => instance.InstanceId) ?? [];

  const users = await supabase
    .from('instances')
    .select('user_id')
    .in('aws_instance_id', instanceIds);
  const userIds = users.data?.map((user) => user.user_id);

  const subtract =
    userIds?.map((user) =>
      supabase.rpc('subtract_credit', {
        user_id: user,
        amount: PRICE_PER_MINUTE,
      })
    ) ?? [];

  await Promise.all(subtract);
}
