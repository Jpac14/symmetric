import { SSTConfig } from 'sst';
import { Cron } from 'sst/constructs';
import * as iam from "aws-cdk-lib/aws-iam";

export default {
  config(_input) {
    return {
      name: 'symmetric',
      region: 'ap-southeast-2',
    };
  },
  stacks(app) {
    const ec2FullAccess = new iam.PolicyStatement({
      actions: ['ec2:*'],
      effect: iam.Effect.ALLOW,
      resources: ['*'],
    })

    const environment = {
      SUPABASE_URL: process.env.NEXT_PUBLIC_SUPABASE_URL!,
      SUPABASE_SERVICE_KEY: process.env.SUPABASE_SERVICE_KEY!,
    }
    
    const stack = app.stack(function Watchdog({ stack }) {
      stack.addDefaultFunctionPermissions([ec2FullAccess]);
      stack.addDefaultFunctionEnv(environment)
      new Cron(stack, 'ChargeRunningTime', {
        schedule: 'rate(1 minute)',
        job: {
          function: {
            handler: 'src/functions/charge-running-time.handler',
          },
        },
      })

      new Cron(stack, 'StopNoCreditInstance', {
        schedule: 'rate(10 minutes)',
        job: {
          function: {
            handler: 'src/functions/stop-no-credit-instance.handler',
          }
        }
      });
    });
  },
} satisfies SSTConfig;
