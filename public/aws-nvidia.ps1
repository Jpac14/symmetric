$Bucket = "nvidia-gaming"
$KeyPrefix = "windows/latest"
$LocalPath = "C:\nvidia-driver\"

Function CurrentDriverVersion {
    return (Get-WmiObject Win32_PnPSignedDriver | where {($_.DeviceName -like "*nvidia*") -and $_.DeviceClass -like "Display"} | Select-Object -ExpandProperty DriverVersion).substring(6,6).replace('.','').Insert(3,'.')
}

Function LatestDriverVersion {
    $Objects = Get-S3Object -BucketName $Bucket -KeyPrefix $KeyPrefix -Region us-east-1
    foreach ($Object in $Objects) {
        $LocalFileName = $Object.Key
        if ($LocalFileName -ne '' -and $Object.Size -ne 0) {
            return $Object.Key.split('/')[2].split('_')[0]
        }
    }
}

Function InstallLatestDriver {
    # download driver
    Write-Host "Downloading latest driver..."
    $Objects = Get-S3Object -BucketName $Bucket -KeyPrefix $KeyPrefix -Region us-east-1
    foreach ($Object in $Objects) {
        $LocalFileName = $Object.Key
        if ($LocalFileName -ne '' -and $Object.Size -ne 0) {
            $LocalFilePath = Join-Path $LocalPath $LocalFileName
            Copy-S3Object -BucketName $Bucket -Key $Object.Key -LocalFile $LocalFilePath -Region us-east-1 | Out-Null
        }
    }

    # install driver
    Write-Host "Installing driver..."
    $InstallerFile = Get-ChildItem -path "$LocalPath\windows\latest" -Include *exe* -Recurse | ForEach-Object { $_.FullName }
    Start-Process -FilePath $InstallerFile -ArgumentList "/s /n" -Wait | Out-Null

    # install license
    New-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\nvlddmkm\Global" -Name "vGamingMarketplace" -PropertyType "DWord" -Value "2" -ErrorAction SilentlyContinue | Out-Null
    Invoke-WebRequest -Uri "https://nvidia-gaming.s3.amazonaws.com/GridSwCert-Archive/GridSwCertWindows_2021_10_2.cert" -OutFile "$Env:PUBLIC\Documents\GridSwCert.txt" -ErrorAction SilentlyContinue | Out-Null
}

Function Cleanup {
    Remove-Item -Path "$LocalPath" -Recurse
}

Write-Host "
   _____                                _        _      
  / ____|                              | |      (_)     
 | (___  _   _ _ __ ___  _ __ ___   ___| |_ _ __ _  ___ 
  \___ \| | | | '_ ` _ \| '_ ` _ \ / _ \ __| '__| |/ __|
  ____) | |_| | | | | | | | | | | |  __/ |_| |  | | (__ 
 |_____/ \__, |_| |_| |_|_| |_| |_|\___|\__|_|  |_|\___|
          __/ |                                         
         |___/                                          
"
Write-Host "Checking for driver updates..."
$CurrentDriverVersion = CurrentDriverVersion
$LatestDriverVersion = LatestDriverVersion

Write-Host ""
Write-Host "Current driver version: $CurrentDriverVersion"
Write-Host "Latest driver version: $LatestDriverVersion"
Write-Host ""

if($CurrentDriverVersion -eq $LatestDriverVersion) {
    Write-Host "There are no updates available"
    Read-Host "Press enter to quit"
    Exit
}

$ReadHost = Read-Host "Do you want to update? (Y/N)"
if ($ReadHost -ne "Y") {
    Write-Host "Not updating GPU driver"
    Read-Host "Press enter to quit"
    Exit
}

Write-Host "Installing driver version: $LatestDriverVersion"
Write-Host ""
Write-Host "The installation may take a few minutes and your instance will be restarted in the process"
Write-Host "If you lose connection, please wait a minute before reconnecting"
Write-Host ""
InstallLatestDriver
Cleanup

Write-Host "Installation complete, your instance will now restart"
Start-Sleep -Seconds 3
Restart-Computer